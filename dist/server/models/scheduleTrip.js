'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Schema = _mongoose2.default.Schema;

var ScheduleTripSchema = new Schema({
    riderId: { type: Schema.Types.ObjectId, ref: 'User' },
    driverId: { type: Schema.Types.ObjectId, ref: 'User' },
    tripId: { type: Schema.Types.ObjectId, ref: 'trip' },
    srcLoc: {
        type: [Number],
        index: '2d'
    },
    destLoc: {
        type: [Number],
        index: '2d'
    },
    tripOtp: { type: Number },
    paymentMode: { type: String, default: 'CASH' },
    carType: { type: String, default: 'Economy' },
    tripRequestStatus: { type: String, default: 'request' },
    tripRequestIssue: { type: String, default: 'noIssues' },
    pickUpAddress: { type: String, default: null },
    destAddress: { type: String, default: null },
    latitudeDelta: { type: Number, default: 0.012 },
    longitudeDelta: { type: Number, default: 0.012 },
    scheduleOn: { type: Date, default: null },
    requestTime: { type: Date, default: Date.now }
});

exports.default = _mongoose2.default.model('ScheduleTripSchema', ScheduleTripSchema);
module.exports = exports['default'];
//# sourceMappingURL=scheduleTrip.js.map
