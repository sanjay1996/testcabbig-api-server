'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _httpStatus = require('http-status');

var _httpStatus2 = _interopRequireDefault(_httpStatus);

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var _APIError = require('../helpers/APIError');

var _APIError2 = _interopRequireDefault(_APIError);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Schema = _mongoose2.default.Schema;

var QuerySchema = new Schema({
    userId: { type: Schema.Types.ObjectId, ref: 'User', default: null },
    querySubject: { type: String, default: null },
    queryMessage: { type: String, default: null },
    queryStatus: { type: String, default: 'Active' },
    submittedOn: { type: Date, default: Date.now }
});

exports.default = _mongoose2.default.model('QuerySchema', QuerySchema);
module.exports = exports['default'];
//# sourceMappingURL=query.js.map
