'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _serverConfig = require('../models/serverConfig');

var _serverConfig2 = _interopRequireDefault(_serverConfig);

var _user = require('../models/user');

var _user2 = _interopRequireDefault(_user);

var _tripRequest = require('../models/trip-request');

var _tripRequest2 = _interopRequireDefault(_tripRequest);

var _smsApi = require('../service/smsApi');

var _smsApi2 = _interopRequireDefault(_smsApi);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/* eslint-disable */
function mobileVerify(req, res, next) {
  _user2.default.findOneAsync({ email: req.query.email })
  //eslint-disable-next-line
  .then(function (foundUser) {
    if (foundUser) {
      var host = req.get('host');
      if (req.protocol + "://" + req.get('host') == "http://" + host) {
        if (req.query.otp == foundUser.otp) {
          _user2.default.findOneAndUpdateAsync({ email: req.query.email }, { $set: { mobileVerified: true } }, { new: true }).then(function (updateUserObj) {
            //eslint-disable-line
            if (updateUserObj) {
              var returnObj = {
                success: true,
                message: 'Mobile verified',
                data: {}
              };
              returnObj.success = true;
              return res.send(returnObj);
            }
          }).error(function (e) {
            var err = new APIError('error in updating user details while login ' + e, httpStatus.INTERNAL_SERVER_ERROR);
            next(err);
          });
        } else {
          var returnObj = {
            success: false,
            message: 'Error Verifying Mobile',
            data: {}
          };
          return res.send(returnObj);
        }
      }
    }
  });
} //eslint-disable-line


function emailVerify(req, res, next) {

  _user2.default.findOneAsync({ email: req.query.email })
  //eslint-disable-next-line
  .then(function (foundUser) {
    if (foundUser) {
      var host = req.get('host');
      console.log(req.protocol + ":/" + req.get('host'));
      if (req.protocol + "://" + req.get('host') == "http://" + host) {
        console.log("Domain is matched. Information is from Authentic email");
        if (req.query.check === foundUser.otp) {
          _user2.default.findOneAndUpdateAsync({ email: req.query.email }, { $set: { emailVerified: true } }, { new: true }) //eslint-disable-line
          .then(function (updateUserObj) {
            //eslint-disable-line
            if (updateUserObj) {
              var returnObj = {
                success: true,
                message: 'Email verified',
                data: {}
              };
              // returnObj.data.user = updateUserObj;
              returnObj.success = true;
              return res.send(returnObj);
            }
          }).error(function (e) {
            var err = new APIError('error in updating user details while login ' + e, httpStatus.INTERNAL_SERVER_ERROR);
            next(err);
          });
          console.log("Email is verified");
          res.end("<h1>Email is been Successfully verified</h1>");
        } else {
          console.log("Email is not verified");
          res.end("<h1>Bad Request</h1>");
        }
      }
    }
  });
}

function isVerified(req, res, next) {
  _user2.default.findOneAsync({ email: req.headers.email }).then(function (foundUser) {
    var returnObj = {
      message: "",
      success: true,
      data: {}
    };
    if (foundUser.mobileVerified == true) {
      returnObj.message = "User Found And Is Verified";
      returnObj.success = true;
      res.send(returnObj);
    } else {
      returnObj.message = "User Found But Not Verified";
      returnObj.success = false;
      res.send(returnObj);
    }
  });
}

function rideVerify(req, res, next) {
  var returnObj = {
    message: "",
    success: false,
    data: {}
  };
  _tripRequest2.default.findById({ _id: req.headers.tripid }).then(function (idfound) {
    if (idfound.tripOtp == req.headers.rideotp) {
      returnObj.message = "otp verified";
      returnObj.success = true;
      res.send(returnObj);
    } else {
      returnObj.message = "otp not verified";
      returnObj.success = false;
      res.send(returnObj);
    }
  });
}

function reSend(req, res) {
  var otpValue = Math.floor(100000 + Math.random() * 900000);
  _user2.default.findOneAndUpdateAsync({ email: req.headers.email }, { $set: { otp: otpValue } }, { new: true }).then(function (foundUser) {
    var returnObj = {
      success: true,
      message: "",
      data: {}
    };
    if (foundUser) {
      returnObj.success = true;
      returnObj.message = "OTP Sent";
      returnObj.data.phoneNo = foundUser.phoneNo;
      if (req.headers.request_type == "ResetPassword") {
        (0, _smsApi2.default)(foundUser._id, "Otp To Reset Password Don't Share" + foundUser.otp, foundUser.phoneNo);
      } else {
        (0, _smsApi2.default)(foundUser._id, "Otp for Mobile Verification " + foundUser.otp, foundUser.phoneNo);
      }
      res.send(returnObj);
    } else {
      returnObj.success = false;
      returnObj.message = "OTP Not Sent";
      res.send(returnObj);
    }
  });
}

exports.default = {
  mobileVerify: mobileVerify,
  emailVerify: emailVerify,
  isVerified: isVerified,
  rideVerify: rideVerify,
  reSend: reSend
};
module.exports = exports['default'];
//# sourceMappingURL=verify.js.map
