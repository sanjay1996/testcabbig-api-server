'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _twilio = require('twilio');

var _twilio2 = _interopRequireDefault(_twilio);

var _serverConfig = require('../models/serverConfig');

var _serverConfig2 = _interopRequireDefault(_serverConfig);

var _user = require('../models/user');

var _user2 = _interopRequireDefault(_user);

var _axios = require('axios');

var _axios2 = _interopRequireDefault(_axios);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function getSmsApiDetails() {
  return new Promise(function (resolve, reject) {
    _serverConfig2.default.findOneAsync({ key: 'smsConfig' }).then(function (foundDetails) {
      resolve(foundDetails.value);
    }).catch(function (err) {
      reject(err);
    });
  });
}

function sendSmsTwilio(userId, smsText) {
  _user2.default.findOneAsync({ _id: userId }).then(function (userObj) {
    getSmsApiDetails().then(function (details) {
      var twilio = new _twilio2.default(details.accountSid, details.token);
      twilio.messages.create({
        from: details.from,
        to: userObj.phoneNo,
        body: smsText
      }, function (err, result) {
        if (err) {
          return err;
        }
        return result;
      });
    });
  });
}

function sendSms(userId, smsText) {
  _user2.default.findOneAsync({ _id: userId }).then(function (userObj) {
    var num = userObj.phoneNo;
    var userMobileNum = num.slice(-10);
    _axios2.default.get('http://103.247.98.91/API/SendMsg.aspx?uname=20171501&pass=Raytheorysms1&send=PRPSCN&dest=' + userMobileNum + '&msg=Hi%20:' + userObj.fname + ',%20Welcome%20to%20CABBIGFamily%20!%20Your%20OTP%20Verification%20Code%20is%20' + smsText).then(function (resp) {
      console.log('====================================');
      console.log(resp.statusText);
      console.log('====================================');
    }).catch(function (e) {
      return console.log("ERROR" + e);
    });
  });
}

exports.default = sendSms;
module.exports = exports['default'];
//# sourceMappingURL=smsApi.js.map
