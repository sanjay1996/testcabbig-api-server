'use strict';

/* eslint-disable */
var printMessage = require('print-message');
var async = require('async');
// Import mongoose.js to define our schema and interact with MongoDB
var mongoose = require('mongoose');

var Schema = mongoose.Schema;
// Import bcrypt-nodejs for hashing passwords on MongoDB
var databaseName = 'cabbig-development';

var nodeEnv = process.env.NODE_ENV;
if (nodeEnv === 'development') {
  databaseName = 'cabbig-development';
}
if (nodeEnv === 'production') {
  databaseName = 'cabbig-production';
}
// URL to connect to a local MongoDB with database test.
// Change this to fit your running MongoDB instance
var databaseURL = 'mongodb://localhost:27017/' + databaseName;

printMessage(['Please have patience while TaxiApp get Installed .This will take around 10 - 15 minutes.'], {
  color: 'green',
  borderColor: 'red'
});

//  AIzaSyAnVhbl1bPiwiJaIc6hoxWf3MZecJijJEU
// Setting up the Token

var ServerConfigSchema = new mongoose.Schema({
  type: { type: Schema.Types.Mixed },
  key: { type: String, required: true, unique: true },
  value: { type: Schema.Types.Mixed }
});

var ServerConfig = mongoose.model('ServerConfig', ServerConfigSchema);

// Async series method to make sure asynchronous calls below run sequentially
async.series([
// function - connect to MongoDB using mongoose, which is an asynchronous call
function (callback) {
  // Open connection to MongoDB
  mongoose.connect(databaseURL);
  // Need to listen to 'connected' event then execute callback method
  // to call the next set of code in the async.serial array
  mongoose.connection.on('connected', function () {
    console.log('db connected via mongoose');
    // Execute callback now we have a successful connection to the DB
    // and move on to the third function below in async.series
    callback(null, 'SUCCESS - Connected to mongodb');
  });
},

// function - use Mongoose to create a User model and save it to database
function (callback) {
  // BEGIN SEED DATABASE
  // Use an array to store a list of User model objects to save to the database
  var serverConfigs = [];
  var serverConfig1 = new ServerConfig({
    type: 'object',
    key: 'cloudinaryConfig',
    value: {
      cloud_name: 'techchefzz',
      api_key: '465552752493661',
      api_secret: 'WFfkL0Qv8Ej7sy_1JW_IBQRZpy0'
    }
  });
  var serverConfig2 = new ServerConfig({
    type: 'object',
    key: 'smsConfig',
    value: {
      accountSid: '',
      token: '',
      form: ''
    }
  });
  var serverConfig3 = new ServerConfig({
    type: 'object',
    key: 'emailConfig',
    value: {
      host: "smtp.gmail.com",
      port: 465,
      secure: true, // secure:true for port 465, secure:false for port 587
      username: "username@gmail.com",
      password: "password"
    }
  });
  // eslint-disable-next-line no-use-before-define
  /*eslint-disable */
  // eslint-disable-next-line no-use-before-define
  var value = serverConfig1;
  var value2 = serverConfig2;
  var value3 = serverConfig3;
  serverConfigs.push(eval(value));
  serverConfigs.push(eval(value2));
  serverConfigs.push(eval(value3));
  // console.log(eval(value));

  /*eslint-disable */
  console.log('Populating database with %s serverConfigs', serverConfigs.length);
  // Use 'async.eachSeries' to loop through the 'users' array to make
  // sure each asnychronous call to save the user into the database
  // completes before moving to the next User model item in the array
  async.eachSeries(
  // 1st parameter is the 'users' array to iterate over
  serverConfigs, function (admin, userSavedCallBack) {
    // There is no need to make a call to create the 'test' database.
    // Saving a model will automatically create the database
    admin.save(function (err) {
      if (err) {
        // Send JSON response to console for errors
        console.dir(err);
      }

      // Print out which user we are saving
      console.log('Saving user #%s', admin.key);

      // Call 'userSavedCallBack' and NOT 'callback' to ensure that the next
      // 'user' item in the 'users' array gets called to be saved to the database
      userSavedCallBack();
    });
  },
  // 3rd parameter is a function to call when all users in 'users' array have
  // completed their asynchronous user.save function
  function (err) {
    if (err) {
      console.log('Finished aysnc.each in seeding db');
    }
    console.log('Finished aysnc.each in seeding db');

    // Execute callback function from line 130 to signal to async.series that
    // all asynchronous calls are now done
    callback(null, 'SUCCESS - Seed database');
  });
  // END SEED DATABASE
}],
// This function executes when everything above is done
function (err, results) {
  console.log('\n\n--- Database seed progam completed ---');

  if (err) {
    console.log('Errors = ');
    console.dir(err);
  } else {
    console.log('Results = ');
    console.log(results);
  }

  console.log('\n\n--- Exiting database seed progam ---');
  // Exit the process to get back to terrminal console
  process.exit(0);
});
//# sourceMappingURL=serverConfig.js.map
