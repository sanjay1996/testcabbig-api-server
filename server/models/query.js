import httpStatus from 'http-status';
import mongoose from 'mongoose';
import APIError from '../helpers/APIError'

const Schema = mongoose.Schema;

const QuerySchema = new Schema({
    userId: { type: Schema.Types.ObjectId, ref: 'User', default: null },
    querySubject: { type: String, default: null },
    queryMessage: { type: String, default: null },
    queryStatus: { type: String, default: 'Active' },
    submittedOn: { type: Date, default: Date.now }
});

export default mongoose.model('QuerySchema', QuerySchema);
