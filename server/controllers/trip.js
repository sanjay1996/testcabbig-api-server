import httpStatus from 'http-status';
import APIError from '../helpers/APIError';
import { fetchReturnObj } from '../service/transform-response';
import TripSchema from '../models/trip';
import ScheduleTrip from "../models/scheduleTrip";

/**
 * Return the trip details of the user.
 * @param req
 * @param res
 * @param next
 * @returns { trip: historyObjArray[{ tripObj }]  }
 */

function getHistory(req, res, next) {
  const historyObjArray = [];
  const userID = req.user._id; //eslint-disable-line
  const userType = req.user.userType;
  const searchObj = {};
  if (userType === 'rider') {
    searchObj.riderId = userID;
  } else if (userType === 'driver') {
    searchObj.driverId = userID;
  }
  TripSchema.find({ $and: [searchObj, { tripStatus: 'endTrip' }] }).sort({ bookingTime: -1 })
    .populate('riderId driverId').then((populatedTrips) => {
      if (populatedTrips.length !== 0) {
        const returnObj = {
          success: true,
          message: 'user trip history ',
          data: populatedTrips
        };
        res.send(returnObj);
      } else {
        const returnObj = {
          success: true,
          message: 'no history available',
          data: []
        };
        res.send(returnObj);
      }
    });
}

function getAllScheduleTrips(req, res, next) {
  ScheduleTrip.find({ riderId: req.headers.user_id }).sort({ scheduleOn: 1 }).populate('riderId').then((tripObj) => {
    if (tripObj.length !== 0) {
      const returnObj = {
        success: true,
        message: 'user trip history',
        data: tripObj
      };
      res.send(returnObj);
    } else {
      const returnObj = {
        success: false,
        message: 'no history available',
        data: []
      };
      res.send(returnObj);
    }
  });
}


function cancelScheduleTrip(req, res, next) {
  ScheduleTrip.findOneAndUpdate({ _id: req.body.tripId }, { $set: { tripRequestStatus: req.body.changeTripStatus } })
    .then((cancelledTripObj) => {
      const returnObj = {
        success: true,
        message: 'Found And Cancelled',
        data: cancelledTripObj
      };
      res.send(returnObj);
    })
}

export default {
  getHistory,
  getAllScheduleTrips,
  cancelScheduleTrip
};




