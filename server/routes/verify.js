import express from 'express';
import verifyCtrl from '../controllers/verify';

const router = express.Router();

router.route('/email')
  .post(verifyCtrl.emailVerify)
  .put(verifyCtrl.emailVerify)
  .get(verifyCtrl.emailVerify);

// /** GET /api/verify/mobileVerify -  */

router.route('/mobile')
  .get(verifyCtrl.isVerified)

  .post(verifyCtrl.mobileVerify);

router.route('/rideVerified')
  .get(verifyCtrl.rideVerify)

router.route('/mobile/reSend')
  .get(verifyCtrl.reSend)
export default router;
